package org.example.service;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CashbackServiceTest {

    @Test
    void shouldCountCorrectly() {
        int percent = 1;
        int taxableSum = 100;
        int limitInKopek = 300000;
        int amountInRubles = 225000;
        int expectedCashbackInKopek = 225000;

        CashbackService service = new CashbackService();
        int actual = service.findOutCashback(percent, taxableSum, limitInKopek, amountInRubles);
        assertEquals(expectedCashbackInKopek, actual);
    }

    @Test
    void shouldRespectTheLimits() {
        int percent = 1;
        int taxableSum = 100;
        int limitInKopek = 300000;
        int amountInRubles = 425000;
        int expectedCashbackInKopek = 300000;

        CashbackService service = new CashbackService();
        int actual = service.findOutCashback(percent, taxableSum, limitInKopek, amountInRubles);
        assertEquals(expectedCashbackInKopek, actual);
    }

    @Test
    void notLessThanNull() {
        int percent = 1;
        int taxableSum = 100;
        int limitInKopek = 300000;
        int amountInRubles = 0;
        int expectedCashbackInKopek = 0;

        CashbackService service = new CashbackService();
        int actual = service.findOutCashback(percent, taxableSum, limitInKopek, amountInRubles);
        assertEquals(expectedCashbackInKopek, actual);
    }
}

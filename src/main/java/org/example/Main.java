package org.example;
import org.example.service.CashbackService;
public class Main {
    public static void main(String[] args) {
        final CashbackService service = new CashbackService();
        final int prediction = service.findOutCashback(2, 100, 300000, 425000);
        System.out.println(prediction);

    }
}

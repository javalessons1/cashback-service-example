package org.example.service;

public class CashbackService {

    public int findOutCashback(int percent, int taxableSum, int limitInKopek, int amount) {
        int kopekBack = taxableSum * percent;
        int howManyTimes = amount / taxableSum;
        int cashbackInKopek = kopekBack*howManyTimes;

        if (cashbackInKopek >= limitInKopek){
            cashbackInKopek = limitInKopek;
        }
        if (cashbackInKopek <= 0){
            return 0;
        }
        return cashbackInKopek;

    }
}

